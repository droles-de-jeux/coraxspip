# CoraxSpip

You'll find here a template for the [SPIP](https://www.spip.net/) publishing system.

This template was designed for the needs of the association [D'rôles De Jeux](http://www.droles-de-jeux.org) and it should adapt easyli for any structure with the same need.

## How to use it

The template is intalled in the directory `squelettes/`.

To know more, you can read [Où placer les fichiers de squelettes ?](https://www.spip.net/fr_article3347.html).

## Dependencies

This template uses Bootstrap 5.2 to allow adaptive display.

The template is also based on several SPIP plugins:

 - [Agenda 2.0 et ultérieur](https://contrib.spip.net/Agenda-2-0-et-ulterieur)
 - [Vérifier](https://contrib.spip.net/Verifier)
 - [Calendrier Mini 2.0](https://contrib.spip.net/Calendrier-Mini-2-0)
 - [ciautoriser : plugin « Pipeline pour autoriser »](https://contrib.spip.net/ciautoriser-plugin-Pipeline-pour-autoriser)
 - [cirv : plugin « rédacteur valideur »](https://contrib.spip.net/cirv-plugin-redacteur-valideur)
 - [Les crayons](https://contrib.spip.net/Les-Crayons)
 - [Facteur](https://contrib.spip.net/Facteur)
 - [Identité Extra](https://contrib.spip.net/Identite-Extra)
 - [Mailsubscribers](https://contrib.spip.net/mailsubscribers)
 - [NoSPAM](https://contrib.spip.net/NoSPAM)
 - [odt2spip : création d’articles à partir de fichiers OpenOffice Writer](https://contrib.spip.net/odt2spip-creation-d-articles-a-partir-de-fichiers)
 - [Réservation d’événements](https://contrib.spip.net/Reservation-d-evenements-4459)
 - [Réservations multiples](https://contrib.spip.net/Reservations-multiples-5016)
 - [Saisies](https://contrib.spip.net/Saisies)
 - [ScssPhp](https://contrib.spip.net/ScssPhp)
 - [Les variantes de squelette](https://www.spip.net/fr_article3445.html)
 - [Le plugin YAML v2](https://contrib.spip.net/Le-plugin-YAML-v2)

## How to contribute

This project is open source, anyone who wishes can contribute freely.

The `main` branch is write-protected and all changes will need to be approved through merge requests.

## License

Currently, all the source code is available un MIT license.
