# CoraxSpip

Vous trouverez ici un squelette pour le CMS [SPIP](https://www.spip.net/).

Ce squelette a été consçu pour répondre aux besoins de l'association [D'rôles De Jeux](http://www.droles-de-jeux.org) et il devrait s'adapter plutôt aisément pour une structure ayant des besoins similaires.

## Installer le squelette

Le squelette s'installe tout simplement dans le dossier `squelettes/` de votre site.

Pour savoir plus, je vous invite à lire [Où placer les fichiers de squelettes ?](https://www.spip.net/fr_article3347.html).

## Les dépendences

A l'heure actuelle, le squelette utilise Bootstrat 5.2 pour permettre au site de s'adapter en fonction de l'écrant d'affichage.

Le squelette se base également sur plusieurs plugins SPIP:

 - [Agenda 2.0 et ultérieur](https://contrib.spip.net/Agenda-2-0-et-ulterieur)
 - [Vérifier](https://contrib.spip.net/Verifier)
 - [Calendrier Mini 2.0](https://contrib.spip.net/Calendrier-Mini-2-0)
 - [ciautoriser : plugin « Pipeline pour autoriser »](https://contrib.spip.net/ciautoriser-plugin-Pipeline-pour-autoriser)
 - [cirv : plugin « rédacteur valideur »](https://contrib.spip.net/cirv-plugin-redacteur-valideur)
 - [Les crayons](https://contrib.spip.net/Les-Crayons)
 - [Facteur](https://contrib.spip.net/Facteur)
 - [Identité Extra](https://contrib.spip.net/Identite-Extra)
 - [Mailsubscribers](https://contrib.spip.net/mailsubscribers)
 - [NoSPAM](https://contrib.spip.net/NoSPAM)
 - [odt2spip : création d’articles à partir de fichiers OpenOffice Writer](https://contrib.spip.net/odt2spip-creation-d-articles-a-partir-de-fichiers)
 - [Réservation d’événements](https://contrib.spip.net/Reservation-d-evenements-4459)
 - [Réservations multiples](https://contrib.spip.net/Reservations-multiples-5016)
 - [Saisies](https://contrib.spip.net/Saisies)
 - [ScssPhp](https://contrib.spip.net/ScssPhp)
 - [Les variantes de squelette](https://www.spip.net/fr_article3445.html)
 - [Le plugin YAML v2](https://contrib.spip.net/Le-plugin-YAML-v2)

## Comment contribuer

Ce projet est open source, tout ce qui le souhaite peuvent contribuer librement.

La branche `main` est protégée en écriture et toute les modifications devront être approuvées par des merge requests.

## Licence

Actuellement, tout le code source est disponible sous licence MIT.
