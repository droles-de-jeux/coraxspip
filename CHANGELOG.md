# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.2] - Unrelease

## [1.0.1] - 2023-04-07

### Fixed

- Using hard coded social links (#14).
- Display of the pagination links (#15).

## [1.0.0] - 2023-04-07

### Added

- Add template version in HTML header meta data (#10)

### Changed

- Hide the newsletter button (#3).
- Renaming main CSS file (#5).
- Update of README.md (#8).
- Displaying event thumbnail in the event card (#9).
- Refactoring of HTML header template (#12).

### Fixed

- First batch of typo correction (#7).
- Buggy display of the footer (#13).
